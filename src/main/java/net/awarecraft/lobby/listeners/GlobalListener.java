package net.awarecraft.lobby.listeners;

import net.awarecraft.api.AwareCraftAPI;
import net.awarecraft.api.events.AsyncUpdateEvent;
import net.awarecraft.api.utils.UpdateUtils;
import net.awarecraft.lobby.AwareCraftLobby;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

/**
 * Created by raulsmail.
 */
public class GlobalListener implements Listener {

    @EventHandler
    public void onAsyncUpdate(AsyncUpdateEvent event) {
        if (event.getUpdateInterval().equals(UpdateUtils.UpdateInterval.FIVE_TICKS)) {
            if (!AwareCraftLobby.getPlugin().getServer().getOnlinePlayers().isEmpty()) {
                AwareCraftLobby.getPlugin().getCommonMethods().getScoreboardUtils().setGlobalScoreboardName(AwareCraftLobby.getPlugin().getCommonMethods().getNextScoreboardName());
            }
        } else if (event.getUpdateInterval().equals(UpdateUtils.UpdateInterval.FIVE_SECONDS)) {
            if (!AwareCraftLobby.getPlugin().getServer().getOnlinePlayers().isEmpty()) {
                AwareCraftLobby.getPlugin().getCommonMethods().setNetworkPlayers(AwareCraftAPI.getPlugin().getCommonMethods().getNetworkPlayers().size());
                for (Player player : AwareCraftLobby.getPlugin().getServer().getOnlinePlayers()) {
                    AwareCraftLobby.getPlugin().getCommonMethods().getScoreboardUtils().addScore(player, "§a" + AwareCraftLobby.getPlugin().getCommonMethods().getNetworkPlayers(), 4);
                }
            }
        }
    }
}
