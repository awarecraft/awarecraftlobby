package net.awarecraft.lobby.listeners;

import com.comphenix.protocol.wrappers.WrappedChatComponent;
import net.awarecraft.api.AwareCraftAPI;
import net.awarecraft.api.objects.User;
import net.awarecraft.api.utils.MessageManager;
import net.awarecraft.api.utils.PlayerUtils;
import net.awarecraft.lobby.AwareCraftLobby;
import net.awarecraft.lobby.utils.LobbyItems;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;

/**
 * Created by raulsmail.
 */
public class PlayerListener implements Listener {

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        User user = PlayerUtils.getUser(event.getPlayer().getUniqueId());
        LobbyItems.LobbyItem optionsItem = AwareCraftLobby.getPlugin().getCommonMethods().getLobbyItems().getOptionsItem();
        event.getPlayer().getInventory().setItem(optionsItem.getDefaultSlot(), optionsItem.getItemBuilder().setDisplayName("§l§r" + MessageManager.getMessageValue(optionsItem.getDatabaseName(), user.getLanguage(), true)).asItemStack());
        AwareCraftLobby.getPlugin().getCommonMethods().createScoreboard(event.getPlayer());
        AwareCraftAPI.getPlugin().getCommonMethods().getTitleManager().displayTitle(event.getPlayer(), WrappedChatComponent.fromJson("{\"text\": \"" + MessageManager.getMessageValue("lobby.join.title", user.getLanguage(), true).replaceAll("%player%", event.getPlayer().getName()) + "\"}"));
        AwareCraftAPI.getPlugin().getCommonMethods().getTitleManager().displaySubtitle(event.getPlayer(), WrappedChatComponent.fromJson("{\"text\": \"" + MessageManager.getMessageValue("lobby.join.subtitle", user.getLanguage(), true) + "\"}"));
        AwareCraftAPI.getPlugin().getCommonMethods().getTitleManager().displayActionBar(event.getPlayer(), WrappedChatComponent.fromJson("{\"text\": \"" + MessageManager.getMessageValue("lobby.join.actionbar", user.getLanguage(), true) + "\"}"));
        AwareCraftAPI.getPlugin().getCommonMethods().getTitleManager().displayPlayerListHeaderFooter(event.getPlayer(), WrappedChatComponent.fromJson("{\"text\": \"" + MessageManager.getMessageValue("lobby.join.header", user.getLanguage(), true) + "\"}"), WrappedChatComponent.fromJson("{\"text\": \"" + MessageManager.getMessageValue("lobby.join.footer", user.getLanguage(), true) + "\"}"));
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
        if (event.getAction().equals(Action.RIGHT_CLICK_AIR) || event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
            if (event.getItem() != null && event.getItem().getType() != null && event.getItem().getItemMeta() != null && event.getItem().getItemMeta().getDisplayName() != null) {
                if (event.getItem().getType().equals(AwareCraftLobby.getPlugin().getCommonMethods().getLobbyItems().getOptionsItem().getItemBuilder().getType())) {
                    AwareCraftLobby.getPlugin().getCommonMethods().getOptionsMenu().openMenu(PlayerUtils.getUser(event.getPlayer().getUniqueId()));
                    event.setCancelled(true);
                }
            }
        }
    }
}
