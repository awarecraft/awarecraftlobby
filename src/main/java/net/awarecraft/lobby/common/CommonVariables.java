package net.awarecraft.lobby.common;

import net.awarecraft.api.utils.ScoreboardUtils;
import net.awarecraft.lobby.menus.OptionsMenu;
import net.awarecraft.lobby.utils.LobbyItems;

import java.util.List;

/**
 * Created by raulsmail.
 */
public abstract class CommonVariables {
    protected ScoreboardUtils scoreboardUtils;
    protected List<String> scoreboardNames;
    protected Integer currentScoreboardName;
    protected LobbyItems lobbyItems;
    protected Integer networkPlayers;
    protected OptionsMenu optionsMenu;
}
