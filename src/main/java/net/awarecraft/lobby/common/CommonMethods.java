package net.awarecraft.lobby.common;

import net.awarecraft.api.AwareCraftAPI;
import net.awarecraft.api.objects.User;
import net.awarecraft.api.utils.MessageManager;
import net.awarecraft.api.utils.PlayerUtils;
import net.awarecraft.api.utils.ScoreboardUtils;
import net.awarecraft.lobby.menus.OptionsMenu;
import net.awarecraft.lobby.utils.LobbyItems;
import org.bukkit.entity.Player;

import java.util.Arrays;

/**
 * Created by raulsmail.
 */
public class CommonMethods extends CommonVariables {

    public void setupVariables() {
        scoreboardUtils = new ScoreboardUtils();
        scoreboardNames = Arrays.asList("§5§lAware§7§lCraft", "§d§lA§5§lware§7§lCraft", "§d§lAw§5§lare§7§lCraft", "§d§lAwa§5§lre§7§lCraft", "§d§lAwar§5§le§7§lCraft", "§d§lAware§7§lCraft", "§d§lAware§f§lC§7§lraft", "§d§lAware§f§lCr§7§laft", "§d§lAware§f§lCra§7§lft", "§d§lAware§f§lCraf§7§lt", "§d§lAware§f§lCraft", "§5§lAware§7§lCraft", "§d§lAware§f§lCraft", "§5§lAware§7§lCraft", "§d§lAware§f§lCraft", "§5§lAware§7§lCraft", "§d§lAware§f§lCraft", "§5§lAware§7§lCraft", "§5§lAware§7§lCraft");
        currentScoreboardName = 0;
        lobbyItems = new LobbyItems();
        networkPlayers = AwareCraftAPI.getPlugin().getCommonMethods().getNetworkPlayers().size();
        optionsMenu = new OptionsMenu();
    }

    public ScoreboardUtils getScoreboardUtils() {
        return scoreboardUtils;
    }

    public LobbyItems getLobbyItems() {
        return lobbyItems;
    }

    public Integer getNetworkPlayers() {
        return networkPlayers <= 0 ? 1 : networkPlayers;
    }

    public void setNetworkPlayers(Integer networkPlayers) {
        this.networkPlayers = networkPlayers;
    }

    public OptionsMenu getOptionsMenu() {
        return optionsMenu;
    }

    public void createScoreboard(Player player) {
        for (int i = 1; i < 15; i++) {
            scoreboardUtils.removeLine(player, i);
        }
        User user = PlayerUtils.getUser(player.getUniqueId());
        scoreboardUtils.addSeparatorScore(player, 15);
        scoreboardUtils.addScore(player, MessageManager.getMessageValue("lobby.scoreboard.rank", user.getLanguage(), true), 14);
        scoreboardUtils.addScore(player, MessageManager.getMessageValue("rank." + user.getRank().getName().toLowerCase() + ".name", user.getLanguage(), true), 13);
        scoreboardUtils.addSeparatorScore(player, 12);
        scoreboardUtils.addScore(player, MessageManager.getMessageValue("lobby.scoreboard.language", user.getLanguage(), true), 11);
        scoreboardUtils.addScore(player, "§b" + MessageManager.getMessageValue("language." + user.getLanguage().getName().toLowerCase() + ".name", user.getLanguage()), 10);
        scoreboardUtils.addSeparatorScore(player, 9);
        scoreboardUtils.addScore(player, "§dAware§eCoins:", 8);
        scoreboardUtils.addScore(player, "§e" + user.getAwareCoins(), 7);
        scoreboardUtils.addSeparatorScore(player, 6);
        scoreboardUtils.addScore(player, MessageManager.getMessageValue("lobby.scoreboard.players", user.getLanguage(), true), 5);
        scoreboardUtils.addScore(player, "§a" + networkPlayers, 4);
        scoreboardUtils.addSeparatorScore(player, 3);
        scoreboardUtils.addScore(player, MessageManager.getMessageValue("lobby.scoreboard.website", user.getLanguage(), true), 2);
        scoreboardUtils.addScore(player, "§awww.awarecraft.net", 1);
    }

    public String getNextScoreboardName() {
        currentScoreboardName++;
        if (currentScoreboardName >= scoreboardNames.size()) {
            currentScoreboardName = 0;
        }
        return scoreboardNames.get(currentScoreboardName);
    }
}
