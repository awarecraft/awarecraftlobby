package net.awarecraft.lobby;

import net.awarecraft.lobby.common.CommonMethods;
import net.awarecraft.lobby.listeners.GlobalListener;
import net.awarecraft.lobby.listeners.PlayerListener;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created by raulsmail.
 */
public class AwareCraftLobby extends JavaPlugin {
    private static AwareCraftLobby plugin;
    private CommonMethods commonMethods;

    public static AwareCraftLobby getPlugin() {
        return plugin;
    }

    @Override
    public void onEnable() {
        plugin = this;
        if (getServer().getPluginManager().isPluginEnabled("AwareCraftAPI")) {
            commonMethods = new CommonMethods();
            commonMethods.setupVariables();
            getServer().getPluginManager().registerEvents(new PlayerListener(), plugin);
            getServer().getPluginManager().registerEvents(new GlobalListener(), plugin);
        } else {
            getLogger().severe("No AwareCraftAPI Was Found! Disabling Plugin.");
            getServer().getPluginManager().disablePlugin(plugin);
        }
    }

    public CommonMethods getCommonMethods() {
        return commonMethods;
    }
}
