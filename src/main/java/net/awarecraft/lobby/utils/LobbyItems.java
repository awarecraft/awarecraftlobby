package net.awarecraft.lobby.utils;

import net.awarecraft.api.utils.ItemBuilder;
import org.bukkit.Material;

/**
 * Created by raulsmail.
 */
public class LobbyItems {
    private LobbyItem optionsItem;

    public LobbyItems() {
        optionsItem = new LobbyItem(8, "lobby.item.options.name", new ItemBuilder(Material.REDSTONE_COMPARATOR));
    }

    public LobbyItem getOptionsItem() {
        return optionsItem;
    }

    public class LobbyItem {
        private Integer defaultSlot;
        private String databaseName;
        private ItemBuilder itemBuilder;

        private LobbyItem(Integer defaultSlot, String databaseName, ItemBuilder itemBuilder) {
            this.defaultSlot = defaultSlot;
            this.databaseName = databaseName;
            this.itemBuilder = itemBuilder;
        }

        public Integer getDefaultSlot() {
            return defaultSlot;
        }

        public String getDatabaseName() {
            return databaseName;
        }

        public ItemBuilder getItemBuilder() {
            return itemBuilder.cloneItemBuilder();
        }
    }
}
