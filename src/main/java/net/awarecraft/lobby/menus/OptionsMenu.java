package net.awarecraft.lobby.menus;

import net.awarecraft.api.objects.User;
import net.awarecraft.api.utils.ItemBuilder;
import net.awarecraft.api.utils.Menu;
import net.awarecraft.lobby.AwareCraftLobby;
import org.bukkit.Material;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

/**
 * Created by raulsmail.
 */
public class OptionsMenu extends Menu {

    public OptionsMenu() {
        super(45, "lobby.inv.options.name", true);
    }

    @Override
    public void putItems(User user, Inventory inventory, Integer integer) {
        inventory.setItem(10, new ItemBuilder(Material.BOOK_AND_QUILL).setDisplayName((user.hasChatEnabled() ? "§a" : "§c") + "add").asItemStack());
        inventory.setItem(19, new ItemBuilder(Material.STAINED_CLAY).setDurability(user.hasChatEnabled() ? (short) 5 : (short) 14).setDisplayName("asd").asItemStack());
        inventory.setItem(40, AwareCraftLobby.getPlugin().getCommonMethods().getLobbyItems().getOptionsItem().getItemBuilder().setDisplayName("Close").asItemStack());
    }

    @Override
    public void clickItemAction(InventoryClickEvent event, ItemStack itemStack, User user) {
        switch (itemStack.getType()) {
            case BOOK_AND_QUILL:
                user.setChatEnabled(!user.hasChatEnabled());
                openMenu(user);
                break;
            case EYE_OF_ENDER:
                user.setPlayersEnabled(!user.hasPlayersEnabled());
                openMenu(user);
                break;
            case BONE:
                user.setPetsEnabled(!user.hasPetsEnabled());
                openMenu(user);
                break;
            case JUKEBOX:
                user.setAlertsEnabled(!user.hasAlertsEnabled());
                openMenu(user);
                break;
            case STAINED_CLAY:
                if (event != null) {
                    ItemStack item = event.getInventory().getItem(event.getSlot() - 9);
                    if (item != null && item.getType() != null) {
                        clickItemAction(null, item, user);
                    }
                }
                break;
            default:
                if (itemStack.getType().equals(AwareCraftLobby.getPlugin().getCommonMethods().getLobbyItems().getOptionsItem().getItemBuilder().getType())) {
                    user.getPlayer().closeInventory();
                }
                break;
        }
    }
}
